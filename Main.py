import queries as qu
import pprint
import random
import sys


# menu functions

# menu with available options for Customer
def menu():
    print('\n\nWelcome ' + usr + "!")
    print(
        'Please select from the following options by entering the number next to it: ')
    print('1. Search')
    print('2. Browse')
    print('3. View Shopping Cart')
    print('4. Profile')
    ans = input()
    if ans == '1':
        search()
    if ans == '2':
        browse()
    if ans == '3':
        view_cart()
    if ans == '4':
        profile()


def search():
    search_query = input('\nSearch: ')
    item_count = 0
    for row in s.query(
            "select product.name, product.category, product.quantityinwh, pricing.price from "
            "(product inner join pricing on product.productid = pricing.productid)"
            " where name like '" +
            '%' + str.upper(str(search_query)) + "%'"):
        item_count += 1
        print('Item: ' + row['name'] + ' Category: ' + row[
            'category'] + ' Quantity: ' + str(row['quantityinwh']) +
              " Price: $" + str(row['price']))
    input(
        str(item_count) + ' result(s) returned. Press Enter to return to menu.')
    menu()


def browse(item=None):
    if item is not None:
        item = item.split()
        cart[str.upper(item[0])] = int(item[1])

        pid = s.query("select productid from product where name=" + "'" +
                      str.upper(item[0]) + "'")[0]['productid']
        price = \
        s.query("select price from pricing where productid=" + str(pid))[0][
            'price']
        cart_id[pid] = [str.upper(item[0]), price, int(item[1])]

        perg = input(
            '\nItem added! If you would like to add another item, enter the item '
            'name followed by the quantity (ex. Apple 2). To return to the menu,'
            ' leave the field empty and press Enter.')
        if perg == '':
            menu()
        else:
            browse(perg)
    else:
        counter = 1
        for row in s.query("select distinct category from product"):
            print(str(counter) + '. ' + row["category"])
            counter += 1
        category = input(
            '\nPlease select a category to browse by name (ex. FRUIT): ')

        item_count = 0
        for row in s.query(
                "select product.name, product.quantityinwh, pricing.price"
                " from (product inner join pricing on product.productid = pricing.productid)"
                " where category="
                + "'" + str.upper(str(category)) + "'"):
            item_count += 1
            print('Item: ' + row['name'] + ' Quantity: ' + str(
                row['quantityinwh'])
                  + ' Price: $' + str(row['price']))
        item_quant = input(str(
            item_count) + ' result(s) returned. To add an item to your cart,'
                          ' enter the item name followed by quantity (ex. Apple 2). '
                          'Leave the field empty and press Enter to return '
                          'to the menu.\n')
        if item_quant == '':
            menu()
        item_quant = item_quant.split()
        cart[str.upper(item_quant[0])] = int(item_quant[1])
        pid = s.query("select productid from product where name=" + "'" +
                      str.upper(item_quant[0]) + "'")[0]['productid']
        price = \
        s.query("select price from pricing where productid=" + str(pid))[0][
            'price']
        cart_id[pid] = [str.upper(item_quant[0]), price, int(item_quant[1])]
        perg = input(
            'Item added! If you would like to add another item, enter the item '
            'name followed by the quantity (ex. Apple 2). To return to the menu,'
            ' leave the field empty and press Enter.\n')
        if perg == '':
            menu()
        else:
            browse(perg)


def view_cart():
    item_count = 0
    print('\n')
    for item in cart.items():
        item_count += item[1]
        print('Item: ' + item[0] + ' Quantity: ' + str(item[1]))
    modify = input(
        str(item_count) + ' items in cart. To add an item, press Enter to'
                          ' return to menu. To remove an item or modify'
                          ' quantity, enter the item name followed by the'
                          ' updated quantity (ex. Apple 0). To complete your order, '
                          'enter Done.')
    if modify == '':
        menu()
    elif modify == 'Done':
        count = 1
        bal = s.query(
            "Select currentbalance from customer where customerid=" + usr_id)[
            0]['currentbalance']
        ord_dets = []
        for item in cart_id.keys():
            order_id = usr_id + str(random.randint(1, 999999))
            s.query("insert into orders values "
                    "(" + order_id + ", " + str(item) + ", " + usr_id +
                    ", " + "(select addressid from customer where customerid=" +
                    usr_id + ")" + ", " + str(cart_id[item][2]) + ", " +
                    str(cart_id[item][2] * cart_id[item][
                        1]) + ", " + "'OPEN', NULL, "
                                     "NULL, NULL)")
            ord_dets.append(cart_id[item][2] * cart_id[item][1])
            bal -= cart_id[item][2] * cart_id[item][1]
            if bal <= 0:
                input(
                    "Uh oh, you cannot afford this purchase order. You are now in debt and your purchase order status is pending until payment.")
                tmp = s.query("update orders set status='PENDING' where"
                              " orderid=" + order_id)
                sys.exit()

            else:

                t = s.query(
                    "update product set quantityinwh=(select quantityinwh from"
                    " product where productid=" + str(item) + ") - " +
                    str(cart_id[item][2]) + " where productid=" + str(item))
                count += 1
        tm = s.query("update customer set currentbalance=" + str(
            bal) + " where customerid=" + usr_id)
        print(
            "Order placed! Review order details below. Thanks for shopping with us! :)")
        print("Total: $" + str(sum(ord_dets)))
        input()
        sys.exit()
    else:
        modify = modify.split()
        if modify[1] == '0':
            del cart[str.upper(modify[0])]
        else:
            cart[str.upper(modify[0])] = int(modify[1])
        view_cart()


def profile():
    for row in s.query("select customer.customername, customer.age, "
                       "address.city, address.st, address.zip, "
                       "address.streetname, address.streetnumber,"
                       " address.aptno, customer.currentbalance,"
                       " customer.ccnumber from"
                       " (customer inner join address on"
                       " customer.addressid = address.addressid)"
                       " where customer.customername=" +
                       "'" + str.upper(usr) + "'"):
        print("Name: " + row['customername'])
        print("Age: " + str(row['age']))
        if row['st'] is None:
            print("Address:")
        elif row['aptno'] is not None:
            print("Address: " + str(row['streetnumber']) + ' ' + row[
                'streetname'] +
                  ' APT ' + str(row['aptno']) + ' ' + row['city'] + ' ' + str(
                row['st']) +
                  ' ' + str(row['zip']))
        else:
            print("Address: " + str(row['streetnumber']) + ' ' + row[
                'streetname'] +
                  ' ' + row['city'] + ' ' + str(
                row['st']) +
                  ' ' + str(row['zip']))
        print('Balance: ' + str(row['currentbalance']))
        print('Credit Card: ' + str(row['ccnumber']))
        res = input(
            '\nIf you would like to change or remove your address or credit card on file,'
            ' enter address or credit card (ex. credit card). To return to the'
            ' menu, leave the field empty and press Enter.\n')
        if res == '':
            menu()
        elif res == 'credit card':
            cc = input(
                'Enter your 16-digit credit card number (ex. 1111111111111111),'
                ' or press Enter to remove your credit card from the '
                'database:\n')
            if cc == '':
                tmp = s.query('update customer set ccnumber=NULL' +
                              ' where customerid=' + "'" + usr_id + "'")
                input(
                    "Credit card successfully removed. Press Enter to return to menu.")
            else:
                tmp = s.query('update customer set ccnumber=' + "'" + cc + "'" +
                              ' where customerid=' + "'" + usr_id + "'")
                input(
                    'Credit card successfully changed. Press Enter to return to menu.')
            menu()
        elif res == 'address':
            rem = input("Would you like to remove your address from the "
                        "database? (y/n): \n")
            if rem == 'y':
                tmp = s.query("update address set "
                              "city=NULL, st=NULL, zip=NULL, streetname=NULL,"
                              " streetnumber=NULL, aptno=NULL where addressid=(select addressid from customer where " \
                              "customerid=" + usr_id + ')')
                input(
                    "Address successfully updated. Press Enter to return to menu.")
            else:

                stno = input('Enter your street number: ')
                stname = input('Enter your street name (ex. Sesame Street): ')
                aptno = input(
                    'Enter your apartment number (leave field empty if not'
                    'applicable): ')
                city = input('Enter your city: ')
                st = input('Enter your state (ex. IL): ')
                zip = input('Enter your 5-digit zip code: ')
                stname = str.upper(stname)
                city = str.upper(city)
                st = str.upper(st)

                if aptno == '':
                    tmp = s.query("update address set "
                                  "city=" + "'" + city + "', " + "st=" + "'" + st + "', " +
                                  "zip=" + "'" + zip + "', " + "streetname=" + "'" + stname + "', " +
                                  "streetnumber=" + "'" + stno + "', " + "aptno=NULL " +
                                  "where addressid=(select addressid from customer where " \
                                  "customerid=" + usr_id + ')')
                    input(
                        "Address successfully updated. Press Enter to return to menu.")
                else:
                    tmp = s.query("update address set "
                                  "city=" + "'" + city + "', " + "st=" + "'" + st + "', " +
                                  "zip=" + "'" + zip + "', " + "streetname=" + "'" + stname + "', " +
                                  "streetnumber=" + "'" + stno + "', " + "aptno=" + "'" + aptno + "' " +
                                  "where addressid=(select addressid from customer where " \
                                  "customerid=" + usr_id + ')')
                    input(
                        "Address successfully updated. Press Enter to return to menu.")

            menu()


def staff():
    print('\nStaff Portal:\n')
    print(
        'Please select from the following options by entering the number next to it: ')
    print('1. Products')
    print('2. Warehouses')
    answ = input()
    if answ == '1':
        products()
    if answ == '2':
        warehouse()


def products():
    q = input("\nWould you like to add a product to the database? (y/n).")
    if q == "y":
        pid = input("Product ID: ")
        name = input("Product Name: ")
        cat = input("Product Category: ")
        wh = input("Warehouse ID: ")
        qinwh = input("Quantity in Warehouse: ")
        qinor = input("Quantity in Order: ")
        alc = input("Alcohol Content: ")
        psize = input("Size: ")
        cal = input("Calories: ")
        ss = input("Serving Size: ")
        sc = input("Sugar Content: ")
        cc = input("Carb Content: ")
        fc = input("Fat Content: ")
        name = str.upper(name)
        cat = str.upper(cat)
        s.query("insert into product values "
                "(" + pid + ", " + "'" + name + "', " + "'" + cat + "', " +
                wh + ", " + qinwh + ", " + qinor + ", " + alc + ", " + psize +
                ", " + cal + ", " + ss + ", " + sc + ", " + cc +
                ", " + fc + ")")
        input("Product added. Press Enter to return to portal.")
        staff()

    else:
        for row in s.query("select * from product"):
            pprint.pprint(row)
        id = input(
            "\nTo modify a product, enter its id number. To return to portal, "
            "leave the field empty and press Enter.")
        if id == '':
            staff()
        else:
            t = input(
                'Would you like to remove this product from the database? (y/n).')
            if t == 'y':
                tmp = s.query("delete from product where productid=" + id)
                input("Product deleted. Press enter to return to portal.")
            else:
                at = input("Enter the attribute to modify (ex. fatcontent): ")
                if at == 'price':
                    state = input(
                        "Enter the state for change in price (ex. IL): ")
                    price = input("Enter new price (ex. 4.99): ")
                    state = str.upper(state)
                    s.query("update pricing set st=" + "'" + state + "', " +
                            "price=" + price + " where productid=" + id)
                    input("Price changed. Press Enter to return to portal.")
                    staff()

                v = input("Enter the modified value for the chosen attribute: ")
                if at == 'name' or at == 'category':
                    tmp = s.query(
                        "update product set " + at + "=" + "'" + str.upper(
                            v) + "'" + " where"
                                       " productid=" + id)
                    input("Product updated. Press Enter to return to portal.")

                else:
                    tmp = s.query(
                        "update product set " + at + "=" + v + " where"
                                                               " productid=" + id)
                    input("Product updated. Press Enter to return to portal.")
            staff()


def warehouse():
    for row in s.query("select * from warehouse"):
        pprint.pprint(row)
    ww = input(
        "\nEnter the warhouse ID of the warehouse you'd like to change storage "
        "capacity. Leave the field empty and press Enter to return to portal.")
    if ww == '':
        staff()
    else:
        wsc = input("Storage Capacity: ")
        tmp = s.query(
            "update warehouse set storcapacity=" + wsc + " where warehouseid="
            + ww)
        input("Storage capacity updated. Press Enter to return to portal.")
        staff()


try:

    session = qu.uri(host='localhost', dbname='postgres', user='postgres',
                     password="jkl;'")
    s = qu.Session(session)
    print('Connected!')
    usr = input('Name: ')  # enter Employee to enter staff portal
    if usr == 'Employee':
        e_id = input("Employee ID: ")
        usr_found = False
        for row in s.query('select employeeid from staffmember'):

            if row['employeeid'] == int(e_id):
                usr_found = True
                usr_id = str(row['employeeid'])
                cart = {}
                cart_id = {}
                staff()
        if not usr_found:
            input('User not found.')
            sys.exit()

    else:
        usr_found = False
        for row in s.query('select customername, customerid from customer'):

            if row['customername'] == str.upper(usr):
                usr_found = True
                usr_id = str(row['customerid'])
                cart = {}
                cart_id = {}
                menu()
        if not usr_found:
            input('User not found.')
            sys.exit()

except:
    print('Uh oh, something went wrong :(')
    input("Press Enter to exit.")
    sys.exit()

#
# with qu.Session(session) as s:
#     for row in s.query('select * from customer'):
#         print(row)
#
